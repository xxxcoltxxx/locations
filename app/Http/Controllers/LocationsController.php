<?php

namespace App\Http\Controllers;

use App\Http\Resources\LocationResource;
use App\Models\Location;
use Services\Locations\LocationsService\LocationsService;

class LocationsController extends Controller
{
    public function index(LocationsService $locationsService)
    {
        $locations = collect($locationsService->getLocations())->mapInto(Location::class);

        return LocationResource::collection($locations);
    }
}
