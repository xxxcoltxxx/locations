<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string name
 * @property array coordinates
 */
class Location extends Model
{
    protected $fillable = [
        'name',
        'coordinates',
    ];

    protected $casts = [
        'coordinates' => 'array',
    ];
}
