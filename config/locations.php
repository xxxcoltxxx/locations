<?php

return [
    'host'    => env('LOCATIONS_HOST', 'https://locations-service.com'),
    'path'    => env('LOCATIONS_PATH', 'locations'),
    'timeout' => env('LOCATIONS_TIMEOUT', 2),
    'client'  => env('LOCATIONS_CLIENT', Services\Locations\LocationsService\HttpLocationsService::class),
];
