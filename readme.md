[![pipeline status](https://gitlab.com/xxxcoltxxx/locations/badges/master/pipeline.svg)](https://gitlab.com/xxxcoltxxx/locations/commits/master)

## Запуск API
Для работы нужен docker и утилита docker-compose

```bash
git clone git@gitlab.com:xxxcoltxxx/locations.git
cd locations
cp .env.example .env

docker-compose up -d
docker-compose exec php composer install
docker-compose exec php php artisan key:generate

# Миграции - одна табличка
docker-compose exec php php artisan migrate

# Миграции для тестовой БД
docker-compose exec php php artisan migrate --env=testing

# Запуск тестов
docker-compose exec php vendor/bin/phpunit -c phpunit.xml

```

* БД слушает порт `15432`
* nginx слушает порт `8080`

API должно быть доступно по адресу http://localhost:8080/api/locations. Если используется docker-machine, то `localhost` нужно заменить на ip докер машины.

## Конфигурация

По умолчанию используется HTTP-клиент (Guzzle), при необходимости можно реализовать что угодно - БД, кеш, массив, файл и т.п. Он ожидает ответ от сервера локаций json в формате:
```json
[{"name":"Eiffel Tower","coordinates":{"lat":21.12,"long":19.56}}]
```

С помощью настроек можно менять клиента, а так же задавать путь и адрес сервера, где локации лежат:
```php
return [
    'host'    => env('LOCATIONS_HOST', 'https://locations-service.com'),
    'path'    => env('LOCATIONS_PATH', 'locations'),
    'timeout' => env('LOCATIONS_TIMEOUT', 2),
    'client'  => env('LOCATIONS_CLIENT', Services\Locations\LocationsService\HttpLocationsService::class),
];
```

P.S. Если честно, задание не совсем понял. Скорее всего то, что я придумал - не то, о чём думали вы. Но надеюсь, что этого достаточно для понимания.

По поводу логирования: при ошибках в канал по-умолчанию пишется ошибка и трейс, поэтому не думаю, что нужно это реализовывать в клиенте.

Еще - вот такие прокси могут привести к недоступности сервиса (например, внешний сервис не отвечает, но слушает порт, и пул php-fpm может забиться соединениями),
поэтому по возможности лучше бы импортировать себе эти локации или придумать способ асинхронной работы с API.
По умолчанию поставил таймаут, чтобы минимизировать возможность недоступности php-fpm.
