<?php

namespace Services\Locations\LocationsService;

class FakeLocationsService implements LocationsService
{
    public function getLocations(): iterable
    {
        return [];
    }
}
