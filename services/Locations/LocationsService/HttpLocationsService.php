<?php

namespace Services\Locations\LocationsService;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use Throwable;

class HttpLocationsService implements LocationsService
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getLocations(): iterable
    {
        try {
            $response = $this->client->get(
                $this->fullUrl(config('locations.path')),
                ['timeout' => config('locations.timeout')]
            );

            $json = json_decode($response->getBody()->getContents(), true);

            throw_unless(
                json_last_error() === JSON_ERROR_NONE,
                new InvalidArgumentException('json_decode error: ' . json_last_error_msg())
            );

            throw_unless(
                is_iterable($json),
                new InvalidArgumentException('response body is not array')
            );

            return $json;
        } catch (Throwable $exception) {
            throw new LocationException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    private function fullUrl(string $url): string
    {
        return sprintf("%s/{$url}", rtrim(config('locations.host'), '/'));
    }
}
