<?php

namespace Services\Locations\LocationsService;

use Exception;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Response;

class LocationException extends Exception implements Responsable
{
    public function toResponse($request)
    {
        return response()->json([
            'success' => false,
            'data'    => [
                'message' => $this->getMessage(),
                'code'    => $this->getCode(),
            ],
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
