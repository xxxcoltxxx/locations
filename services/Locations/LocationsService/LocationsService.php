<?php

namespace Services\Locations\LocationsService;

interface LocationsService
{
    public function getLocations(): iterable;
}
