<?php

namespace Services\Locations;

use Illuminate\Support\ServiceProvider;
use Services\Locations\LocationsService\LocationsService;

class LocationsServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function register()
    {
        $this->app->bind(LocationsService::class, config('locations.client'));
    }

    public function provides()
    {
        return [LocationsService::class];
    }
}
