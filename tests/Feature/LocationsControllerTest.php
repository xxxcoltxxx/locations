<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Mockery;
use Services\Locations\LocationsService\FakeLocationsService;
use Services\Locations\LocationsService\LocationException;
use Services\Locations\LocationsService\LocationsService;
use Tests\TestCase;

class LocationsControllerTest extends TestCase
{
    private $successBody = [
        [
            'name'        => 'Eiffel Tower',
            'coordinates' => ['lat' => 21.12, 'long' => 19.56],
        ],
    ];

    public function test_success_response_format()
    {
        $client = Mockery::mock(FakeLocationsService::class)->makePartial();
        $client->shouldReceive('getLocations')->once()->andReturn($this->successBody);
        $this->app->instance(LocationsService::class, $client);

        $response = $this->getJson(route('locations.index'));

        $response->assertSuccessful();
        $response->assertJson(['data' => $this->successBody]);
    }

    public function test_error_response_format()
    {
        $client = Mockery::mock(FakeLocationsService::class)->makePartial();
        $client->shouldReceive('getLocations')->once()->andThrow(LocationException::class, 'some error', 10);
        $this->app->instance(LocationsService::class, $client);

        $response = $this->getJson(route('locations.index'));

        $response->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
        $response->assertJson([
            'data'    => ['message' => 'some error', 'code' => 10],
            'success' => false,
        ]);
    }
}
