<?php

namespace Tests\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

/**
 * @mixin TestCase
 */
trait GuzzleMockTrait
{
    protected function mockResponse($data, $statusCode)
    {
        $headers = ['Content-Type' => 'application/json'];
        $body = json_encode($data);
        $mock = new MockHandler([
            new Response($statusCode, $headers, $body),
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $this->app->instance(Client::class, $client);
    }
}
