<?php

namespace Tests\Unit;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Services\Locations\LocationsService\HttpLocationsService;
use Services\Locations\LocationsService\LocationException;
use Services\Locations\LocationsService\LocationsService;
use Tests\Helpers\GuzzleMockTrait;
use Tests\TestCase;

class HttpLocationsServiceTest extends TestCase
{
    use GuzzleMockTrait;

    private $successBody = [
        [
            'name'        => 'Eiffel Tower',
            'coordinates' => ['lat' => 21.12, 'long' => 19.56],
        ],
    ];

    protected function setUp()
    {
        parent::setUp();
        Config::set('locations.client', HttpLocationsService::class);
        $this->app->bind(LocationsService::class, config('locations.client'));
    }

    public function test_client_from_config_is_applied()
    {
        $this->assertInstanceOf(HttpLocationsService::class, $this->getLocationService());
    }

    public function test_return_format()
    {
        $this->mockResponse($this->successBody, Response::HTTP_OK);

        $locations = $this->getLocationService()->getLocations();

        $this->assertEquals($this->successBody, (array) $locations);
    }

    public function test_malformed_json()
    {
        $this->mockResponse('not a json', Response::HTTP_OK);

        $this->expectException(LocationException::class);
        $this->expectExceptionMessage('response body is not array');

        $this->getLocationService()->getLocations();
    }

    public function test_exception_on_response_error()
    {
        $this->mockResponse($this->successBody, Response::HTTP_BAD_REQUEST);

        $this->expectException(LocationException::class);
        $this->expectExceptionMessage('400 Bad Request');

        $this->getLocationService()->getLocations();
    }

    private function getLocationService(): LocationsService
    {
        return app(LocationsService::class);
    }
}
